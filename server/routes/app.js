const express = require('express');
const router = express.Router();

const history = require('express-history-api-fallback');
router.use(history("app.html", { root: './client/build/html' }))

const _ = undefined;

const _v2 = require('../lib/v2.js');
_v2.io(client => {
  let session = client.sessionid;
  let ip = _v2.fn.parseIPAddress(client.request.connection.remoteAddress);

  client.on('disconnect', () => {
    _v2.fn.clientDisconnect(session, client);
  })

  client.on('/v2/connect.io', async(data, fn) => {
    try {
      _v2.fn.clientConnect(session, client);
   
      data.session = session;
      data.client = client.id;
      data.ip = ip;

      _v2.log.debug(`[${ip}][${session}][${client.id}] Looking up for authentication token...`);
      if (data.auth) {        
        data.auth = `$2a$12$${data.auth}`;
        
        _v2.log.debug(`[${ip}][${session}][${client.id}] Validating authentication token...`);
        return fn(await _v2.fn.userAuthentication(data));
      }
      return fn(await _v2.fn.guestAuthentication(data));
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/connect.io returned the error: `, error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/chat/load.io', async(data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      return fn(await _v2.db.getChatData(data.limit));
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/chat/load.io returned the error: `, error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/anime/load.io', async(data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      return fn(await _v2.db.getAnimeDataBy());
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/anime/load.io returned the error: `,  error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/anime/load.specific.io', async(data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      let results = await _v2.db.getAnimeDataBy({
        where: { identifier: "LinkID", value: data.anime }
      }, { 
        users: true 
      })

      return fn(results);
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/anime/load.specific.io returned the error: `, error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/episode/load.io', async(data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      let results = await _v2.db.getEpisodeDataBy(data.anime, _, { users: true });

      return fn(results);
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/anime/load.specific.io returned the error: `, error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/episode/load.specific.io', async (data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      let results = await _v2.db.getEpisodeDataBy(data.anime, {
        where: { identifier: "EpisodeID", value: data.episode }
      }, { 
        users: true 
      });

      return fn(results);
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/anime/load.specific.io returned the error: `, error);
      return fn({ fail: error });
    }
  })

  client.on('/v2/search/user.io', async (data, fn) => {
    try {
      if (!_v2.fn.isClientConnected(session, client.id)) { throw "Client invalid, ai fost deconectat! Te rugăm reîncearcă in câteva secunde sau reîncarcă pagina"; }
      let results = await _v2.db.findUsers(data.find.split('&'));

      return fn(results);
    }
    catch (error) {
      _v2.log.error(`[${ip}][${session}][${client.id}] /v2/search/user.io returned the error: `, error);
      return fn({ fail: error });
    }
  })
})

function parseCookies(request) {
  var list = {},
    rc = request.headers.cookie;

  rc && rc.split(';').forEach(function (cookie) {
    var parts = cookie.split('=');
    list[parts.shift().trim()] = decodeURI(parts.join('='));
  })
  return list;
}

module.exports = router;
