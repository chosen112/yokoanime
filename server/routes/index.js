const express = require('express');
const router = express.Router();

const history = require('express-history-api-fallback');
router.use(history("main.html", { root: './client/build/html' }))

/*router.get('/', function(req, res) {
	return res.sendFile('main.html', { root: './client/build/html' });
})*/

function parseCookies(request) {
	var list = {},
			rc = request.headers.cookie;

	rc && rc.split(';').forEach(function( cookie ) {
		var parts = cookie.split('=');
		list[parts.shift().trim()] = decodeURI(parts.join('='));
	})
	return list;
}
module.exports = router;
