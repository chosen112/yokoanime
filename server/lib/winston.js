var log = require('winston');

var filedate = function () {
  let timestamp = new Date();
  return timestamp.getFullYear() + '-' + ('0' + (timestamp.getMonth() + 1)).slice(-2) + '-' + ('0' + timestamp.getDate()).slice(-2) + '_' + ('0' + timestamp.getHours()).slice(-2) + '-' + ('0' + timestamp.getMinutes()).slice(-2) + '-' + ('0' + timestamp.getSeconds()).slice(-2) + '_';
}
var timestamp = function () {
  let timestamp = new Date();
  return '[' + ('0' + timestamp.getDate()).slice(-2) + '/' + ('0' + (timestamp.getMonth() + 1)).slice(-2) + '/' + timestamp.getFullYear() + ' - ' + ('0' + timestamp.getHours()).slice(-2) + ':' + ('0' + timestamp.getMinutes()).slice(-2) + ':' + ('0' + timestamp.getSeconds()).slice(-2) + ']';
}

log.configure({
  transports: [
    new (log.transports.Console)({
      level: 'trace',
      prettyPrint: true,
      colorize: true
    }),
    new (log.transports.File)({
      name: 'info-file',
      filename: './server/logs/' + filedate() + 'yokoanime-log.log',
      level: 'trace',
      json: false,
      timestamp: timestamp
    }),
    new (log.transports.File)({
      name: 'error-file',
      filename: './server/logs/error/' + filedate() + 'yokoanime-err.log',
      level: 'error',
      json: false,
      timestamp: timestamp
    })
  ]
})

log.setLevels({
  trace: 9,
  input: 8,
  verbose: 7,
  prompt: 6,
  debug: 5,
  info: 4,
  data: 3,
  help: 2,
  warn: 1,
  error: 0
})

log.addColors({
  trace: 'magenta',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  debug: 'blue',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  error: 'red'
})

let _debug = log.debug;
log.debug = function () {
  if (!log.debugging) return;
  let ret = _debug.apply(this, arguments);

  delete _debug;
  return ret;
}

let _trace = log.trace;
log.trace = function () {
  if (!log.tracing) return;
  let ret = _trace.apply(this, arguments);

  delete _trace;
  return ret;
}

module.exports = log;