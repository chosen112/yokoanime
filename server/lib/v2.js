// Bit Operators
// Checking: 1110 & 1<<0 == false; 1110 & 1<<2 == true;
// Add 1: 0000 |= 1<<2 == 0100;
// Remove 1: 1111 &= ~(1<<2) = 1011

const bcrypt = require('bcryptjs');
const io = require('./io.js');
const knex = require('./knex.js');

const _ = undefined;

const _v2 = {
  log: require('./winston.js'),
  io: init,  
  schema: {
    // anime, manga, movie & ova
    link: `d_tbl_link_v2`,
    anime: `d_tbl_anime_v2`,
    episode: `d_tbl_episode_v2`,
    episode_source: `d_tbl_episode_source_v2`,
    manga: `d_tbl_manga_v2`,
    movie: `d_tbl_movie_v2`,
    ova: `d_tbl_ova_v2`,
    coordinator: `d_tbl_coordinator_v2`,
    encoder: `d_tbl_encoder_v2`,
    translator: `d_tbl_translator_v2`,
    verifier: `d_tbl_verifier_v2`,
    genre: `d_tbl_genre_v2`,
    season: `d_tbl_season_v2`,
    tag: `d_tbl_tag_v2`,
    // user
    access_custom: `d_tbl_access_custom_v2`,
    access: `d_tbl_access_v2`,
    guest: `d_tbl_guest_v2`,
    guest_ip: `d_tbl_guest_ip_v2`,
    guest_log: `d_tbl_guest_log_v2`,    
    user_access: `d_tbl_user_access_v2`,
    user_avatar: `d_tbl_user_avatar_v2`,
    user_email: `d_tbl_user_email_v2`,
    user_ip: `d_tbl_user_ip_v2`,
    user_log: `d_tbl_user_log_v2`,
    user_name: `d_tbl_user_name_v2`,
    user_personal: `d_tbl_user_personal_v2`,
    user: `d_tbl_user_v2`,
    // chat
    chat: `d_tbl_chat_v2`
    /*
    access: "d_tbl_access_v2",
    ban: "d_tbl_ban_v2",
    emoticons: "d_tbl_emoticons_v2",
    anime: "d_tbl_anime_v2",
    movies: "d_tbl_movies_v2",
    episodes: "d_tbl_episodes_v2",
    users: "d_tbl_users_v2",
    guests: "d_tbl_guests_v2",
    chat: "d_tbl_chat_v2",
    news: "d_tbl_news_v2",
    comments: "d_tbl_comments_v2",
    reports: "d_tbl_reports_v2",
    stats: "d_tbl_stats_v2",
    patch: "d_tbl_patch_v2",
    announcements: "d_tbl_announcements_v2",
    notifications: "d_tbl_notifications_v2"*/
  },
  data: {
    connections: 0,
    $clients: { }
  },
  db: {
    async getUserDataByAuth(auth) {
      let result = await knex.raw(require('./queries/getUserDataByAuth.qry')(_v2.schema), [auth]);
      return (result[0].length) ? _v2.db.cleanResult(result[0][0]) : null;
    },
    async getGuestDataBySession(session) {
      let result = await knex.raw(require('./queries/getGuestDataBySession.qry')(_v2.schema), [session]);
      return (result[0].length) ? _v2.db.cleanResult(result[0][0]) : null;
    },
    async getAnimeDataBy(options={}, get={}) {
      let time_before = Date.now();

      let bind = { };
      if (options.where) {
        bind.where = `a.${options.where.identifier}`;
        bind.value = options.where.value
      }
      bind.order = (options.order = options.order || { by: "JPTitle" }).by;

      queries = require('./queries/getAnimeDataBy.qry');
      let results = (await knex.raw(queries('data', _v2.schema, options), bind))[0];
      
      for(let i = 0; i < results.length; i++) {
        results[i].Genre = (results[i].Genre) ? results[i].Genre.split('|') : [];
        results[i].Tags = (results[i].Tags) ? results[i].Tags.split('|') : [];

        if (get.users) {
          bind = [
            results[i].Coordinators = results[i].Coordinators.split('|').initial(''),
            results[i].Encoders = results[i].Encoders.split('|').initial(''),
            results[i].Translators = results[i].Translators.split('|').initial(''),
            results[i].Verifiers = results[i].Verifiers.split('|').initial('')
          ]

          let parsed = (await knex.raw(queries('full', _v2.schema), bind))[0];
          for (let j = 0; j < parsed.length; j++) {
            results[i][parsed[j].which].shift();
            results[i][parsed[j].which].push(parsed[j]);
          }
        }

        if (get.episodes) {
          results[i].EpisodeList = await _v2.db.getEpisodeDataBy(results[i].LinkID, {
            where: { identifier: "EpisodeID", value: results[i].EpisodeID }
          }, {
            users: true
          })
        }
      }

      _v2.log.trace(`Loading time for getAnimeDataBy: ${Date.now() - time_before}ms`);
      return results;
    },
    async getEpisodeDataBy(id, options={}, get={}) {
      let time_before = Date.now();

      let bind = {};
      bind.id = id;
      if (options.where) {
        bind.where = `e.${options.where.identifier}`;
        bind.value = options.where.value
      }
      bind.order = (options.order = options.order || { by: "Episode" }).by;

      queries = require('./queries/getEpisodeDataBy.qry');
      let results = (await knex.raw(queries('data', _v2.schema, options), bind))[0];
      
      if (get.users) {
        for (let i = 0; i < results.length; i++) {
          bind = [
            results[i].Encoders = results[i].Encoders.split('|').initial(''),
            results[i].Translators = results[i].Translators.split('|').initial(''),
            results[i].Verifiers = results[i].Verifiers.split('|').initial('')
          ]

          let parsed = (await knex.raw(queries('full', _v2.schema), bind))[0];
          for (let j = 0; j < parsed.length; j++) {
            results[i][parsed[j].which].shift();
            results[i][parsed[j].which].push(parsed[j]);
          }
        }
      }
      _v2.log.trace(`Loading time for getEpisodeDataBy: ${Date.now() - time_before}ms`);
      return results;
    },
    async getChatData() {
      let results = (await knex.raw(require('./queries/getChatData.qry')(_v2.schema)))[0];
      return results;
    },
    async findUsers(find) {
      find = find.filter((val) => val.trim().length); // remove empty strings

      let bind = find.map((val) => (val.charAt(0) == ':') ? `${val.slice(1)}` : `%${val}%`); // find id if starts with query starts with :
      let result = await knex.raw(require('./queries/findUsers.qry')(_v2.schema, find), bind);
      return (result[0].length) ? result[0] : null;
    },
    cleanResult(data) {
      // Remove keys matching: ^Session | ^Key$
      for (let key in data) {
        if (key.match(/(^Session$|^Key$)/)) delete data[key];
      }
      return data;
    }
  },
  fn: {
    deleteSession(session) {
      delete _v2.data.$clients[session];
    },
    //
    addClientToSession(session, client) {
      _v2.data.$clients[session][client] = { };
    },
    removeClientFromSession(session, client) {
      delete _v2.data.$clients[session][client];

      _v2.log.trace(`[purge] Client ${client} on session ${session} has been erased`);
      _v2.log.trace(`[purge] Remaining clients on the session ${session}: ${_v2.fn.countClientsOnSession(session)}`);
    },
    countClientsOnSession(session) {
      let c = 0;
      for (let key in _v2.fn.getClientsOnSession(session)) {
        c++;
      }
      if (!c) _v2.fn.deleteSession(session);

      return c;
    },
    getClientsOnSession(session) {
      return _v2.data.$clients[session] || { };
    },
    getClient(session, client) {
      return _v2.data.$clients[session][client];
    },
    isClientConnected(session, client) {
      return (_v2.data.$clients[session] && _v2.data.$clients[session][client])
    }, 
    clientConnect(session, client) {
      _v2.data.connections++;
      _v2.data.$clients[session] = _v2.fn.getClientsOnSession(session);
      _v2.fn.addClientToSession(session, client.id);
      
      _v2.log.trace(`[connection] Client ${client.id} on session ${session} is using User-Agent:${client.request.headers['user-agent']}`);
      _v2.log.trace(`[connection] Total clients on the session ${session}: ${_v2.fn.countClientsOnSession(session)}`);
    },
    clientDisconnect(session, client) {
      if (_v2.fn.isClientConnected(session, client.id)) {
        _v2.log.trace(`[disconnection] Client ${client.id} on session ${session} was using User-Agent:${client.request.headers['user-agent']}`);
        _v2.log.trace(_v2.fn.getClient(session, client.id));

        _v2.fn.removeClientFromSession(session, client.id);
        _v2.data.connections--;
      }
    },
    async clientTouch(as, ip, result) {
      let id = result[`${as}ID`];
      result.DateAccessed = Date.now();
      result.DateCreated = parseInt(result.DateCreated || result.DateAccessed);
      result.NumAccessed = (result.NumAccessed || 0) + 1;
      result.IPAddress = ip;

      let queries = require(`./queries/add${as}ToClient.qry`);
      knex.transaction(async (trx) => {
        try {
          await trx.raw(queries(1, _v2.schema), [[id, result.DateCreated, result.DateAccessed, result.NumAccessed]]);
          await trx.raw(queries(2, _v2.schema), [[id, result.IPAddress, result.DateAccessed]]);
        }
        catch (err) {
          throw err;
        }
      })
      return result;
    },
    async addUserToClient(session, client, ip, result) {
      _v2.data.$clients[session][client] = { 
        user: { UserID: result.UserID, IPAddress: ip }
      }

      _v2.log.debug(`[${ip}][${session}][${client}] User ${result.UserID} client session has been updated`);
      return await _v2.fn.clientTouch("User", ip, result);
    },
    async addGuestToClient(session, client, ip, result) {
      _v2.data.$clients[session][client] = {
        guest: { GuestID: result.GuestID, IPAddress: ip }
      }

      _v2.log.debug(`[${ip}][${session}][${client}] Guest ${result.GuestID} client session has been updated`);
      return await _v2.fn.clientTouch("Guest", ip, result);
    },
    async userAuthentication(data) {
      let result = await _v2.db.getUserDataByAuth(data.auth);
      if (result) {
        _v2.log.info(`[${data.ip}][${data.session}][${data.client}] Authentication token validated!`);
        return await _v2.fn.addUserToClient(data.session, data.client, data.ip, result);

        //results.Bans = await func_v2.Database.getUserBans(results.Id, BAN_ALL, true);
        //results.Notifications = await func_v2.Database.getUserNotifications(results.Id);
        //return result;
      }

      _v2.log.warn(`[${data.ip}][${data.session}][${data.client}] Authentication token is not genuine`);
      return await _v2.fn.guestAuthentication(data);
    },
    async guestAuthentication(data) {
      _v2.log.debug(`[${data.ip}][${data.session}][${data.client}] Looking up for guest session data`);

      let result = await _v2.db.getGuestDataBySession(data.session);
      if (result) {
        _v2.log.debug(`[${data.ip}][${data.session}][${data.client}] Guest ${result.GuestID} session data found`);
        return await _v2.fn.addGuestToClient(data.session, data.client, data.ip, result);
      }

      _v2.log.warn(`[${data.ip}][${data.session}][${data.client}] Guest session could not be found in database`);
      let guest = { 
        GuestID: (await knex.raw(require('./queries/addGuestToDatabase.qry')(_v2.schema), [data.session]))[0].insertId
      }

      _v2.log.debug(`[${data.ip}][${data.session}][${data.client}] Guest ${guest.GuestID} has been created and added to database`);
      return await _v2.fn.addGuestToClient(data.session, data.client, data.ip, guest);
    },
    //
    parseIPAddress(remoteAddress) {
      return remoteAddress.substring(remoteAddress.lastIndexOf(':') + 1);
    },
  }
}

async function batchInsert(table, records, options={}) {
  if (!table) throw "No table specified!";
  if (!records || !records.length) throw "No records specified!";

  let columns = [];
  for (let key in records[0]) {
    columns.push(key);
  }
  vals = records.map(record => {
    let tmp = [];
    for (let key in record) {
      tmp.push(record[key]);
    }
    return tmp;
  })
  
  if (!columns.length || !vals.length) throw `Records are invalid | Sample: ${records[0]}`;

  let sqlDuplicate = (options.duplicate) ? `ON DUPLICATE KEY ${options.duplicate.do} ${options.duplicate.data.map(data => `${data.column} = ${data.value}`).join(',')}` : "";
  let updateQuery = `INSERT INTO ${table} (${columns.map(column => `\`${column}\``).join(',')}) VALUES ${records.map(() => '(?)').join(',')} ${sqlDuplicate}`;  

  return await knex.raw(updateQuery, vals);
}

async function migrate() {
  let results, time_before, time_start = Date.now(), insertidx;
  let tmp, data = { };

  /*
  data = { 
    tbl_user: [],
    tbl_user_ip: [],
    tbl_user_email: [],
    tbl_user_access: [],
    tbl_user_name: [],
    tbl_user_avatar: [],
    tbl_user_log: [],
    tbl_user_personal: []
  }
  
  time_before = Date.now();
  results = await knex(`tbl_users_dev`).select();
  for (let i = 0; i < results.length; i++) {
    data.tbl_user.push({ UserID: results[i].Id, Username: results[i].Username, Key: results[i].Auth });
    
    let logs = JSON.parse(results[i].Logs);

    tmp = JSON.parse(results[i].ipAddress);
    data.tbl_user_ip = data.tbl_user_ip.concat(tmp.filter(ip => ip.length).map((k) => { return (k.length) ? { UserID: results[i].Id, IPAddress: k, IPDate: logs.dateJoined } : false }));
    data.tbl_user_email.push({ UserID: results[i].Id, EMailAddress: results[i].EMail, EMailDate: logs.dateJoined });

    tmp = JSON.parse(results[i].Access);
    data.tbl_user_access.push({ UserID: results[i].Id, AccessID: tmp.Id, Custom: "00000", AccessDate: logs.dateJoined });

    tmp = JSON.parse(results[i].Profile);
    data.tbl_user_name.push({ UserID: results[i].Id, Name: tmp.Nickname, NameDate: logs.dateJoined });
    data.tbl_user_avatar.push({ UserID: results[i].Id, Avatar: false, AvatarDate: logs.dateJoined });    

    data.tbl_user_log.push({ UserID: results[i].Id, DateCreated: logs.dateJoined, DateAccessed: logs.dateSeen, NumAccessed: logs.loggedNum });
    data.tbl_user_personal.push({ UserID: results[i].Id });
  }
  await batchInsert(_v2.schema.user, data.tbl_user);
  await batchInsert(_v2.schema.user_ip, data.tbl_user_ip, { duplicate: { do: "UPDATE", data: [{ column: 'Date', value: 'VALUES(Date)' }] } });
  await batchInsert(_v2.schema.user_email, data.tbl_user_email);
  await batchInsert(_v2.schema.user_access, data.tbl_user_access);
  await batchInsert(_v2.schema.user_log, data.tbl_user_log);
  await batchInsert(_v2.schema.user_personal, data.tbl_user_personal);
  await batchInsert(_v2.schema.user_name, data.tbl_user_name);
  await batchInsert(_v2.schema.user_avatar, data.tbl_user_avatar);
  _v2.log.trace("Loading time for migrate()->tbl_users_dev: %d ms", Date.now() - time_before);
  */

  /*
  data = {
    tbl_anime: [],
    tbl_episode: [],
    tbl_episode_source: [],
    tbl_genre: [],
    tbl_season: [],
    tbl_tag: [],
    tbl_coordinators: [],
    tbl_verifiers: [],
    tbl_encoders: [],
    tbl_translators: [],
  }

  time_before = Date.now();
  data.tbl_link = [];
  let animes = await knex(`tbl_anime_dev`).select();
  for (let i = 0; i < animes.length; i++) {
    data.tbl_link.push({ which: "anime" });
  }
  insertidx = (await batchInsert(_v2.schema.link, data.tbl_link))[0].insertId;
  for (let i = 0; i < animes.length; i++) {
    let anim_title = JSON.parse(animes[i].Title);
    let anim_desc = JSON.parse(animes[i].Description);    

    data.tbl_anime.push({ LinkID: insertidx+i, Episodes: animes[i].Episodes || 0, Duration: animes[i].Duration, Rating: animes[i].Rating,
                          Cover: animes[i].Image, Trailer: animes[i].Video, myAnimeList: animes[i].myAnimeList,
                          JPTitle: anim_title.original, ENTitle: anim_title.english, RODesc: anim_desc.ro, ENDesc: anim_desc.en });

    tmp = animes[i].Genre.split(",");
    data.tbl_genre = data.tbl_genre.concat(tmp.map((k) => { return { LinkID: insertidx+i, Genre: k.trim() } } ));

    data.tbl_coordinators = data.tbl_coordinators.concat(animes[i].Coordinator.match(/(\w+)/g).map((k) => { return { link: insertidx+i, Name: k.trim(), Since: animes[i].dateAdded } }))
    data.tbl_encoders = data.tbl_encoders.concat(animes[i].Encoders.match(/(\w+)/g).map((k) => { return { link: insertidx + i, Name: k.trim(), Since: animes[i].dateAdded } }))
    data.tbl_verifiers = data.tbl_verifiers.concat(animes[i].Verifiers.match(/(\w+)/g).map((k) => { return { link: insertidx+i, Name: k.trim(), Since: animes[i].dateAdded } }))    
    data.tbl_translators = data.tbl_translators.concat(animes[i].Translators.match(/(\w+)/g).map((k) => { return { link: insertidx+i, Name: k.trim(), Since: animes[i].dateAdded } }))

    let episodes = await knex(`tbl_episodes_dev`).select().where('animeId', animes[i].animeId);
    for (let j = 0; j < episodes.length; j++) {
      let ep_title = JSON.parse(episodes[j].Title);
      let ep_desc = JSON.parse(episodes[j].Description);

      data.tbl_episode.push({ LinkID: insertidx+i, ROTitle: ep_title.ro, ENTitle: ep_title.en, Episode: episodes[j].Episode, Schedule: episodes[j].Schedule,
                              Cover: episodes[j].Image, Trailer: episodes[j].Video, RODesc: ep_desc.ro, ENDesc: ep_desc.en, Source: episodes[j].Source,
                              Encoders: episodes[j].Encoder, Verifiers: episodes[j].Verifiers, Translators: episodes[j].Translators });
    }
  }

  //episodes
  data.tbl_link = [];
  for (let i = 0; i < data.tbl_episode.length; i++) {
    data.tbl_link.push({ which: "episode" });
  }
  insertidx = (await batchInsert(_v2.schema.link, data.tbl_link))[0].insertId;
  for (let i = 0; i < data.tbl_episode.length; i++) {
    let sources = JSON.parse(data.tbl_episode[i].Source);
    let encoders = data.tbl_episode[i].Encoders.match(/(\w+)/g);
    let verifiers = data.tbl_episode[i].Verifiers.match(/(\w+)/g);
    let translators = data.tbl_episode[i].Translators.match(/(\w+)/g);

    delete data.tbl_episode[i].Source;
    delete data.tbl_episode[i].Encoders;
    delete data.tbl_episode[i].Verifiers;
    delete data.tbl_episode[i].Translators;

    data.tbl_episode[i].EpisodeID = insertidx+i;
    data.tbl_episode_source = data.tbl_episode_source.concat(sources.filter(source => source.length).map((source, index) => { return { EpisodeID: insertidx+i, SourceOrder: index+1, SourceRef: source } }));

    data.tbl_encoders = data.tbl_encoders.concat(encoders.map((k) => { return { link: insertidx+i, Name: k.trim(), Since: data.tbl_episode[i].Schedule } }))
    data.tbl_verifiers = data.tbl_verifiers.concat(verifiers.map((k) => { return { link: insertidx+i, Name: k.trim(), Since: data.tbl_episode[i].Schedule } }))
    data.tbl_translators = data.tbl_translators.concat(translators.map((k) => { return { link: insertidx+i, Name: k.trim(), Since: data.tbl_episode[i].Schedule } }))
  }

  for (let i = 0; i < data.tbl_coordinators.length; i++) {
    let rs = await knex.raw(`SELECT UserID FROM ${_v2.schema.user_name} WHERE \`Name\` = ?`, [data.tbl_coordinators[i].Name]);
    if (!rs[0].length) {
      console.dir(`Name: ${data.tbl_coordinators[i].Name} is undefined`);
      continue;
    }
    data.tbl_coordinators.splice(i, 1, { UserID: rs[0][0].UserID, LinkID: data.tbl_coordinators[i].link, CoordinatingDate: data.tbl_coordinators[i].Since })
  }
  for (let i = 0; i < data.tbl_verifiers.length; i++) {
    let rs = await knex.raw(`SELECT UserID FROM ${_v2.schema.user_name} WHERE \`Name\` = ?`, [data.tbl_verifiers[i].Name]);
    if (!rs[0].length) {
      console.dir(`Name: ${data.tbl_verifiers[i].Name} is undefined`);
      continue;
    }
    data.tbl_verifiers.splice(i, 1, { UserID: rs[0][0].UserID, LinkID: data.tbl_verifiers[i].link, VerifyingDate: data.tbl_verifiers[i].Since })
  }
  for (let i = 0; i < data.tbl_encoders.length; i++) {
    let rs = await knex.raw(`SELECT UserID FROM ${_v2.schema.user_name} WHERE \`Name\` = ?`, [data.tbl_encoders[i].Name]);
    if (!rs[0].length) {
      console.dir(`Name: ${data.tbl_encoders[i].Name} is undefined`);
      continue;
    }
    data.tbl_encoders.splice(i, 1, { UserID: rs[0][0].UserID, LinkID: data.tbl_encoders[i].link, EncodingDate: data.tbl_encoders[i].Since })
  }
  for (let i = 0; i < data.tbl_translators.length; i++) {
    let rs = await knex.raw(`SELECT UserID FROM ${_v2.schema.user_name} WHERE \`Name\` = ?`, [data.tbl_translators[i].Name]);
    if (!rs[0].length) {
      console.dir(`Name: ${data.tbl_translators[i].Name} is undefined`);
      continue;
    }
    data.tbl_translators.splice(i, 1, { UserID: rs[0][0].UserID, LinkID: data.tbl_translators[i].link, TranslatingDate: data.tbl_translators[i].Since })
  }
  await batchInsert(_v2.schema.anime, data.tbl_anime);
  await batchInsert(_v2.schema.episode, data.tbl_episode);
  await batchInsert(_v2.schema.episode_source, data.tbl_episode_source);
  await batchInsert(_v2.schema.genre, data.tbl_genre);
  await batchInsert(_v2.schema.coordinator, data.tbl_coordinators);
  await batchInsert(_v2.schema.verifier, data.tbl_verifiers);
  await batchInsert(_v2.schema.encoder, data.tbl_encoders);
  await batchInsert(_v2.schema.translator, data.tbl_translators);
  _v2.log.trace("Loading time for migrate()->tbl_anime_dev: %d ms", Date.now() - time_before);
  */

  _v2.log.trace("Total loading time for migrate(): %d ms", Date.now() - time_start);
}

async function init(ack) {  
  _v2.log.debugging = true;
  _v2.log.tracing = true;
  _v2.log.trace('::executing init() function');

  io.v2.on('connection', ack);
  try {
    await migrate();
    //await loadAccess();
    //await loadStats();

    /*let file = await asyncfs.open('./www/sitemap.xml', 'w', 0777);
    await asyncfs.appendFile(file, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\r\n', "utf8");
    asyncfs.appendFile(file, '    <url>\r\n        <loc>http://app.yokoanime.com/</loc>\r\n    </url>\r\n', "utf8");
    let sitemap_urls;
    sitemap_urls = await $knex(func_v2.$Tables.episodes).select();
    for(let result of sitemap_urls) {
      await asyncfs.appendFile(file, '    <url>\r\n        <loc>http://app.yokoanime.com/anime/a' + result.animeId + '/episode/e' + result.episodeId + '</loc>\r\n    </url>\r\n', "utf8");
    }
    sitemap_urls = await $knex(func_v2.$Tables.anime).select();
    for(let result of sitemap_urls) {
      await asyncfs.appendFile(file, '    <url>\r\n        <loc>http://app.yokoanime.com/anime/a' + result.animeId + '</loc>\r\n    </url>\r\n', "utf8");
    }
    await asyncfs.appendFile(file, '</urlset>', "utf8");
    await asyncfs.close(file);		*/
  }
  catch (error) {
    _v2.log.error(`async init() returned the error: ${error}`);
  }
}

Array.prototype.initial = function(val) {
  return (this.length) ? this : [val]
}

module.exports = _v2;