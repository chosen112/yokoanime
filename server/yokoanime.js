const express = require('express');

const path = require('path');
const favicon = require('serve-favicon');
const mime = require('mime-types');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();

const subdomain = require('express-subdomain');
const expressSession = require('express-session');
const knexSession = require('connect-session-knex')(expressSession);
const mysql = require('mysql');

const io = require('./lib/io.js');
const knex = require('./lib/knex.js');
const winston = require('./lib/winston.js');

const sessionSecret = '9xqMTrMVehuXZ9gqrtzhbBMnJAzgLYKVuFuvy7c8zJ4vuyES';
const sharingSession = expressSession({
	secret: sessionSecret,
	resave: true,
	saveUninitialized: true,
	cookie: {
		maxAge: 60*60*24*365*1000
	},
	store: new knexSession({ 
		knex: knex,
		clearInterval: 900000
	})
})
app.use(sharingSession);
app.use(cookieParser(sessionSecret));

// view engine setup
app.set('views', path.join(__dirname, 'html'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/assets', express.static(path.join(__dirname, '../client/build/assets'), { maxAge: '1y' }));
app.use(express.static(path.join(__dirname, '../client/build'), { maxAge: 0 }));
app.use(favicon(path.join(__dirname, '../client/build/assets/favicon.ico')));

app.set('port', process.env.PORT || 80);
const server = require("http").createServer(app);

io.server = require("socket.io")(server);
io.v2 = io.server.of('/v2');

const routes = {
	index: require('./routes/index.js'),
	app: require('./routes/app.js')
}
app.use(subdomain('app', routes.app));
app.use('/', routes.index);

io.v2.use(function(socket, next) {
	cookieParser(sessionSecret)(socket.request, null, function (err, data) {
		socket.sessionid = socket.request.signedCookies['connect.sid'];
	})
  next();
})

server.listen(app.get("port"));
winston.info('IO server is now listening on port %d', app.get('port'));