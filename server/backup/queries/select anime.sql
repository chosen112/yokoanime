CREATE TABLE d_tbl_manga_v2 LIKE d_tbl_anime_v2; 
INSERT newtable SELECT * FROM oldtable;

SELECT
	which,
	anime.LinkID as id
FROM
	d_tbl_anime_v2 anime
    NATURAL LEFT JOIN
		d_tbl_link_v2
	INNER JOIN
		d_tbl_genre_v2 anime_genre on anime.LinkID = anime_genre.LinkID and anime_genre.Genre LIKE "%Action%"	
UNION ALL
SELECT
	which,
	ova.LinkID as id
FROM
	d_tbl_ova_v2 ova
    NATURAL LEFT JOIN
		d_tbl_link_v2
	INNER JOIN
		d_tbl_genre_v2 ova_genre on ova.LinkID = ova_genre.LinkID and ova_genre.Genre LIKE "%Action%";
        
        

SELECT
	*
FROM
	d_tbl_anime_v2 a 
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(Tag separator '|') Tag
		 FROM
				d_tbl_tag_v2
		 GROUP BY
				LinkID
		) a_tag
	NATURAL LEFT JOIN
		d_tbl_season_v2 a_season
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(Genre separator '|') Genre
		 FROM
				d_tbl_genre_v2
		 GROUP BY
				LinkID
		) a_genre
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(UserID separator '|') CoordinatorIDs
		 FROM
				d_tbl_coordinator_v2
		 GROUP BY
				LinkID
		) a_coordinators
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(UserID separator '|') EncoderIDs
		 FROM
				d_tbl_encoder_v2
		 GROUP BY
				LinkID
		) a_encoders
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(UserID separator '|') TranslatorIDs
		 FROM
				d_tbl_translator_v2
		 GROUP BY
				LinkID
		) a_translators
	NATURAL LEFT JOIN
		(SELECT
				LinkID,
				group_concat(UserID separator '|') VerifierIDs
		 FROM
				d_tbl_verifier_v2
		 GROUP BY
				LinkID
		) a_verifiers;



SELECT
	a.*,
    a_genre.Genre,
    a_tag.Tag,
    a_season.SeasonName,
    a_season.SeasonNumber,
    a_coordinators.CoordinatorIDs,
    a_encoders.EncoderIDs,
    a_translators.TranslatorIDs,
    a_verifiers.VerifierIDs
FROM
	d_tbl_movie_v2 a 
	LEFT JOIN
		(SELECT
				MovieID,
				group_concat(Tag separator '|') Tag
		 FROM
				d_tbl_movie_tag_v2
		 GROUP BY
				MovieID
		) as a_tag ON a_tag.MovieID = a.MovieID
	LEFT JOIN
		d_tbl_anime_season_v2 a_season ON a_season.MovieID = a.AnimeID
	LEFT JOIN
		(SELECT
				AnimeID,
				group_concat(Genre separator '|') Genre
		 FROM
				d_tbl_anime_genre_v2
		 GROUP BY
				AnimeID
		) as a_genre ON a_genre.AnimeID = a.AnimeID
	LEFT JOIN
		(SELECT
				CoordinatingID,
				group_concat(UserID separator '|') CoordinatorIDs
		 FROM
				d_tbl_coordinator_v2
		 GROUP BY
				CoordinatingID
		) a_coordinators ON a_coordinators.CoordinatingID = concat("anime",a.AnimeID)
	LEFT JOIN
		(SELECT
				EncodingID,
				group_concat(UserID separator '|') EncoderIDs
		 FROM
				d_tbl_encoder_v2
		 GROUP BY
				EncodingID
		) a_encoders ON a_encoders.EncodingID = concat("anime",a.AnimeID)
	LEFT JOIN
		(SELECT
				TranslatingID,
				group_concat(UserID separator '|') TranslatorIDs
		 FROM
				d_tbl_translator_v2
		 GROUP BY
				TranslatingID
		) a_translators ON a_translators.TranslatingID = concat("anime",a.AnimeID)
	LEFT JOIN
		(SELECT
				VerifyingID,
				group_concat(UserID separator '|') VerifierIDs
		 FROM
				d_tbl_verifier_v2
		 GROUP BY
				VerifyingID
		) a_verifiers ON a_verifiers.VerifyingID = concat("anime",a.AnimeID)
WHERE a.AnimeID IN (28)