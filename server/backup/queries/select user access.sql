SELECT
	which,
    UserID,
	Name,
    AccessName,
    AccessLevel,
    AccessCSS
FROM    
	(SELECT
		UserID,
		'c' as which
	FROM
		d_tbl_user_v2 u
	WHERE
		UserID IN (3)
	UNION ALL
	SELECT
		UserID,
		'e' as which
	FROM
		d_tbl_user_v2 u_name
	WHERE
		UserID IN (2, 3)
	UNION ALL
	SELECT
		UserID,
		't' as which
	FROM
		d_tbl_user_v2 u_name
	WHERE
		UserID IN (2, 3, 6)
	UNION ALL
	SELECT
		UserID,
		'v' as which
	FROM
		d_tbl_user_v2 u_name
	WHERE
		UserID IN (2, 3)
	) unified
	NATURAL JOIN
		d_tbl_user_name_v2 u_name
    NATURAL JOIN
		d_tbl_user_access_v2 u_access
    LEFT JOIN
		d_tbl_access_v2 access on access.AccessID = u_access.AccessID
		