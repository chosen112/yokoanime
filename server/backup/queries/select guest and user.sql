SELECT
	*
FROM
	d_tbl_user_v2 u
    NATURAL JOIN
		d_tbl_user_access_v2 u_ac
	NATURAL JOIN
		(SELECT r_av.* FROM d_tbl_user_avatar_v2 r_av 
			INNER JOIN (SELECT UserID, MAX(AvatarDate) maxdate FROM d_tbl_user_avatar_v2 GROUP BY UserID) in_av on (in_av.UserID = r_av.UserID and in_av.maxdate = r_av.AvatarDate)
		) u_av
	NATURAL JOIN
		(SELECT r_em.* FROM d_tbl_user_email_v2 r_em
			INNER JOIN (SELECT UserID, MAX(EMailDate) maxdate FROM d_tbl_user_email_v2 GROUP BY UserID) in_em on (in_em.UserID = r_em.UserID and in_em.maxdate = r_em.EMailDate)
		) u_em
	NATURAL JOIN
		(SELECT r_ip.* FROM d_tbl_user_ip_v2 r_ip
			INNER JOIN (SELECT UserID, MAX(IPDate) maxdate FROM d_tbl_user_ip_v2 GROUP BY UserID) in_ip on (in_ip.UserID = r_ip.UserID and in_ip.maxdate = r_ip.IPDate)
		) u_ip
	NATURAL JOIN
		d_tbl_user_log_v2 u_log
	NATURAL JOIN
		(SELECT r_nm.* FROM d_tbl_user_name_v2 r_nm
			INNER JOIN (SELECT UserID, MAX(NameDate) maxdate FROM d_tbl_user_name_v2 GROUP BY UserID) in_nm on (in_nm.UserID = r_nm.UserID and in_nm.maxdate = r_nm.NameDate)
		) u_nm
	NATURAL JOIN
		d_tbl_user_personal_v2 u_ps        
WHERE
	u.Key = '$2a$12$KJ8wU8Hh7plNMuuPO1CJk.rT2qaWaKUZwpTPNyOWJgoinkG/cYw9O';

    
SELECT
	*
FROM
	d_tbl_guest_v2 g
	NATURAL JOIN
		(SELECT r_ip.* FROM d_tbl_guest_ip_v2 r_ip
			INNER JOIN (SELECT GuestID, MAX(IPDate) maxdate FROM d_tbl_guest_ip_v2 GROUP BY GuestID) in_ip on (in_ip.GuestID = r_ip.GuestID and in_ip.maxdate = r_ip.IPDate)
		) g_ip
	NATURAL JOIN
		d_tbl_guest_log_v2 g_log
WHERE
	g.Session = 'T8VXi3prvmTSUIWZCGWziUzcFRAoJsJV';