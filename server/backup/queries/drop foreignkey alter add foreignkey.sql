
    /*anime: `d_tbl_anime_v2`,
    coordinator: `d_tbl_coordinator_v2`,
    encoder: `d_tbl_encoder_v2`,
    translator: `d_tbl_translator_v2`,
    verifier: `d_tbl_verifier_v2`,
    genre: `d_tbl_genre_v2`,
    season: `d_tbl_season_v2`,
    tag: `d_tbl_tag_v2`,*/
    
ALTER TABLE `yokoan5_main`.`d_tbl_anime_v2` 
DROP FOREIGN KEY `fk_anime_link_id`, 
DROP INDEX `fk_anime_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_coordinator_v2` 
DROP FOREIGN KEY `fk_coordinator_link_id`, 
DROP INDEX `fk_coordinator_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_encoder_v2` 
DROP FOREIGN KEY `fk_encoder_link_id`, 
DROP INDEX `fk_encoder_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_translator_v2` 
DROP FOREIGN KEY `fk_translator_link_id`,
DROP INDEX `fk_translator_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_verifier_v2` 
DROP FOREIGN KEY `fk_verifier_link_id`,
DROP INDEX `fk_verifier_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_genre_v2` 
DROP FOREIGN KEY `fk_genre_link_id`,
DROP INDEX `fk_genre_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_season_v2` 
DROP FOREIGN KEY `fk_season_link_id`,
DROP INDEX `fk_season_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_tag_v2` 
DROP FOREIGN KEY `fk_tag_link_id`,
DROP INDEX `fk_tag_link_id_idx`;

ALTER TABLE `yokoan5_main`.`d_tbl_link_v2` 
CHANGE COLUMN `LinkID` `LinkID` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `yokoan5_main`.`d_tbl_anime_v2` 
ADD INDEX `fk_anime_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_anime_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_coordinator_v2` 
ADD INDEX `fk_coordinator_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_coordinator_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

ALTER TABLE `yokoan5_main`.`d_tbl_encoder_v2` 
ADD INDEX `fk_encoder_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_encoder_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_translator_v2` 
ADD INDEX `fk_translator_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_translator_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_verifier_v2` 
ADD INDEX `fk_verifier_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_verifier_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_genre_v2` 
ADD INDEX `fk_genre_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_genre_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_season_v2` 
ADD INDEX `fk_season_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_season_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
ALTER TABLE `yokoan5_main`.`d_tbl_tag_v2` 
ADD INDEX `fk_tag_link_id_idx` (`LinkID` ASC),
ADD CONSTRAINT `fk_tag_link_id`
  FOREIGN KEY (`LinkID`)
  REFERENCES `yokoan5_main`.`d_tbl_link_v2` (`LinkID`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  


