SELECT
	*
FROM
	d_tbl_episode_v2 e
	NATURAL LEFT JOIN
		(SELECT
				EpisodeID,
				group_concat(SourceRef separator '|') SourceRef
		 FROM
				d_tbl_episode_source_v2
		 GROUP BY
				EpisodeID 	 
		) e_source
	NATURAL LEFT JOIN
		(SELECT
				LinkID as EpisodeID,
				group_concat(UserID separator '|') EncoderIDs
		 FROM
				d_tbl_encoder_v2
		 GROUP BY
				EpisodeID
		) e_encoders
	NATURAL LEFT JOIN
		(SELECT
				LinkID as EpisodeID,
				group_concat(UserID separator '|') TranslatorIDs
		 FROM
				d_tbl_translator_v2
		 GROUP BY
				EpisodeID
		) e_translators
	NATURAL LEFT JOIN
		(SELECT
				LinkID as EpisodeID,
				group_concat(UserID separator '|') VerifierIDs
		 FROM
				d_tbl_verifier_v2
		 GROUP BY
				EpisodeID
		) e_verifiers
WHERE
	e.AnimeID = 1;
