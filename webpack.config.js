const path = require('path');
const config = {
  mode: 'development',
  entry: './client/src/main.js',
  output: {
    path: path.resolve(__dirname, 'client/build'),
    filename: 'main.build.js'
  },
  resolve: {
    modules: [ path.resolve(__dirname, 'node_modules') ],
    alias: {
      'assets': path.join(__dirname, 'client/build/assets'),
      'styles': path.resolve(__dirname, 'client/src/styles'),
      'data': path.resolve(__dirname, 'client/build/data')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/i,
        use: {
          loader: 'vue-loader'
        }
      },
      {
        test: /\.js$/i,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env'],
            plugins: ['transform-vue-jsx', 'babel-plugin-transform-runtime']
          }
        }
      },      
      {
        test: /\.(gif|png|jpe?g|svg|ttf|eot|woff|woff2)$/i,
        loader: 'url-loader'
      },
      {
        test: /\.scss$/,
        use: [ 
          'style-loader', 
          'css-loader', 
          {
            loader: 'sass-loader',
            options: {
              includePaths: [path.resolve(__dirname, 'client/src/styles')],
              outputStyle: 'compressed'
            }
          }
        ]
      }
    ]
  }
}

module.exports = config;