const LANG_DAYS = ["Duminică", "Luni", "Marți", "Miercuri", "Joi", "Vineri", "Sâmbătă"];
const LANG_MONTHS = ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"];

const util = {
  timestamp: {
    toString(timestamp) {
      return `${LANG_DAYS[timestamp.getDay()]}, ${timestmap.getDate()} ${LANG_MONTHS[timestamp.getMonth()]} ${timestamp.getFullYear()} ${timestamp.getHours().toString().padStart(2, "0")} ${timestamp.getMinutes().toString().padStart(2, "0")}`;
    },
    timeFormat: function (timestamp) {
      timestamp = Math.floor(timestamp / 1000);

      if (timestamp < 30) return 0;
      let time, secs, mins, hours, days;
      secs = timestamp % 60;
      time = pad(secs, 2) + "s";
      timestamp = Math.floor(timestamp / 60);
      if (timestamp < 1) return time;
      mins = timestamp % 60;
      time = pad(mins, 2) + "m " + time;
      timestamp = Math.floor(timestamp / 60)
      if (timestamp < 1) return time;
      hours = timestamp % 24;
      time = pad(hours, 2) + "h " + time;
      timestamp = Math.floor(timestamp / 24);
      if (timestamp < 1) return time;
      days = timestamp;
      time = days + "d " + time;

      return time;
    },
    timeString: function (timestamp) {
      timestamp = Math.floor(timestamp / 1000);

      if (timestamp == 0) return "Acum";
      else if (0 < timestamp && timestamp < 60) return "Acum " + timestamp + ((timestamp == 1) ? " secundă" : " secunde");
      else if (60 <= timestamp && timestamp < 3600) return "Acum " + Math.floor(timestamp / 60) + ((Math.floor(timestamp / 60) == 1) ? " minut" : " minute");
      else if (3600 <= timestamp && timestamp < 86400) return "Acum " + Math.floor(timestamp / 3600) + ((Math.floor(timestamp / 3600) == 1) ? " oră" : " ore");
      else if (86400 <= timestamp && timestamp < 31536000) return "Acum " + Math.floor(timestamp / 86400) + ((Math.floor(timestamp / 86400) == 1) ? " zi" : " zile");
      else if (31536000 <= timestamp) return "Acum " + Math.floor(timestamp / 31536000) + ((Math.floor(timestamp / 31536000) == 1) ? " an" : " ani");
    }
  },

  object: {
    copy(to, from) {
      return Object.assign(to, (from) ? JSON.parse(JSON.stringify(from)) : from);
    },
    clone(to, from) {
      return Object.assign(to, from);
    }
  },

  str: {
    days: ['lu', 'ma', 'mi', 'jo', 'vi', 'sâ', 'du'],
    months: ['ianuarie', 'februarie', 'martie', 'aprilie', 'mai', 'iunie', 'iulie', 'august', 'septembrie', 'octombrie', 'noiembrie', 'decembrie'],
    date(timestamp, time=false) {
      let date = new Date(timestamp);

      let string = `${(date.getDate().toString().padStart(2, '0'))} ${util.str.months[date.getMonth()+1]} ${date.getFullYear()}`;
      if (time) string = string + ` ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;

      return string;
    }
  },

  uuid_v4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0, v = (c == 'x') ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    })
  },
  nthChild(element) {
    for(var i = -1; (element = element.previousSibling); i++);
    return i+1;
  },
  hasClass(element, className) {
    if ((` ${element.className} `).replace(/[\n\t\r]/g, " ").indexOf(` ${className} `) > -1) {
      return true;
    }
    return false;
  },
  selector(el) {
    let computedSelector = "";
    do {
      if (el.tagName == "BODY") break;

      let currentSelector = el.tagName.toLowerCase();
      if (el.id) currentSelector += `#${el.id}`;
      if (el.className) currentSelector += `.${el.className.split(' ').join('.')}`;
      computedSelector = `${currentSelector} ${computedSelector}`;
    } while (el = el.parentElement)

    return computedSelector;
  },
  offset(el) {
    let left = 0;
    let top = 0;

    do {
      left += el.offsetLeft;
      top += el.offsetTop;
    } while (el = el.offsetParent)

    return { left, top }
  },
  IsInt(num) {
    return num % 1 === 0;
  },
  IsFloat(num) {
    return num % 1 !== 0;
  },
  IsEmpty(data) {
    for (let i in data) {
      return false;
    }

    return true;
  },
  getDomain(url) {
    let matches = url.match(/(?:^https?\:\/\/)?([^\/?#]+)\./i);
    let domain = matches[1].split('.');
    
    return domain[domain.length - 1];
  },
  getRandomInt(min, max) {
    return Math.floor((Math.random() * Math.floor(max - min)) + min)
  },
  clampMin(n, min) {
    return (n < min) ? min : n;
  },
  clampMax(n, max) {
    return (n > max) ? max : n;
  },
  clampRange(n, start, end) {
    return (n < start) ? start : (n > end) ? end : n
  },  
  clampLoop(n, start, end) {
    return (n < start) ? end : (n > end) ? start : n;
  }
}

export default util;