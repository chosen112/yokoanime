import Vue from 'vue/dist/vue.esm.js'

require('./vue.components.js');
require('./vue.directives.js');

import store from './vue.store.js';
import router from './vue.routes.js';

window.onload = function() {
  new Vue({
    store,
    router,
    el: 'main#vue-app',
    template: '<App />'
  })
}