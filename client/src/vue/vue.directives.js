import Vue from 'vue/dist/vue.esm.js'

import store from './vue.store.js';

Vue.directive('tooltip', {
  bind(el, binding) {
    store.commit('createTooltip', { id: el, color: binding.value.color, text: binding.value.text });
  },
  unbind(el) {
    store.commit('destroyTooltip', { id: el });
  }
})