import Vue from 'vue/dist/vue.esm.js'
import Vuex from 'vuex';
Vue.use(Vuex);

import io from '../io.loader.js';
import util from '../util.funcs.js';

const store = new Vuex.Store({
  state: {
    session: {},
    anime: [],
    ova: [],
    movie: [],
    manga: [],
    UI: {
      windows: [],
      notifications: [],
      tooltips: { 
        all: { },
        visible: null
      },
      errors: {
        input: []
      },
      sidebar: true,
      social: true
    }
  },
  mutations: {
    createTooltip(state, tooltip) {
      let el = tooltip.id;
      if (!el) return console.error("Invalid tooltip element");

      tooltip.element = util.selector(el);
      let show = () => this.commit('showTooltip', { element: tooltip.element });
      let hide = () => this.commit('hideTooltip');
      el.addEventListener('mouseenter', show);
      el.addEventListener('mouseleave', hide);
      state.UI.tooltips.all[tooltip.element] = tooltip;
    },
    destroyTooltip(state, tooltip) {
      tooltip.element = util.selector(el);
      delete state.UI.tooltips.all[tooltip.element];
    },
    showTooltip(state, tooltip) {
      state.UI.tooltips.visible = state.UI.tooltips.all[tooltip.element];
    },
    hideTooltip(state) {
      state.UI.tooltips.visible = null;
    },
    async loadAnime(state) {
      let result = await io.async.emit('/v2/anime/load.io');
      state.anime = [];
      for (let i = 0; i < result.length; i++) {
        state.anime.push(result[i]);
      }
    },
    async loadOVA(state) {

    },
    async loadMovie(state) {

    },
    async loadManga(state) {

    },
    createNotification(state, notification) {
      let id = util.uuid_v4();
      state.notifications.push({ id, message: notification.message, type: notification.type });

      setTimeout(this.commit, 10000, 'deleteNotification', { id })
    },
    deleteNotification(state, notification) {
      let id = state.notifications.map(notification => notification.id).indexOf(notification.id);
      if (id != -1) state.notifications.splice(id, 1);
    },
    //
    getActiveWindow(state) {
      return (state.UI.windows.length) ? (state.UI.windows.length - 1) : null;
    },
    getWindowByUUID(state, window) {
      if (window.id) return this.commit('getActiveWindow');

      let id = state.UI.windows.map((win) => win.id).indexOf(window.id);
      return (id != -1) ? id : this.commit('getActiveWindow');
    },
    addWindow(state, window) {
      const defaults = {
        id: util.uuid_v4(),
        title: 'Unknown title',
        text: 'Unknown text',
        dismissable: true,
        draggable: true,
        name: '',
        min: {
          width: 0.5,
          height: null
        },
        max: {
          width: 0.5,
          height: 0.5
        },
        escapable: true,
        modal: false,
        async OnClose() { },
        async OnDismiss() { }
      }
      options = Object.assign(defaults, window.options);
      state.UI.windows.push(options);
    },
    deleteWindow(state, window) {
      state.UI.windows.splice(window.id, 1);
    },
    async closeWindow(state, window) {
      let id = this.commit('getWindowByUUID', { id: window.id });
      if (id == null) return;

      let allow = await state.UI.windows[id].OnClose(data);
      if (allow === false) return; // prevent closing

      this.commit('deleteWindow', { id });
    },
    async dismissWindow(state, window) {
      let id = this.getWindowByUUID(window.id);
      if (id == null) return;

      let allow = await state.UI.windows[id].OnDismiss(data);
      if (allow === false) return; // prevent dismissing

      this.commit('deleteWindow', { id });
    },
    focusWindow(state, window) {
      let id = this.getWindowByUUID(window.id);
      if (id == null || id == state.UI.windows.length-1) return; // window is invalid/already focused

      state.UI.windows.push(state.UI.windows.splice(id, 1)[0]);
    }
  }
})

module.exports = store;