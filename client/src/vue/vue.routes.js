import Vue from 'vue/dist/vue.esm.js'
import vueRouter from 'vue-router/dist/vue-router.js';
Vue.use(vueRouter);

import vp404 from './modules/pages/404.page.vue';
import vpHome from './modules/pages/home.page.vue';
import vpNews from './modules/pages/news.page.vue';
import vpAnime from './modules/pages/anime/anime.index.vue';
import vpAnimeSpecific from './modules/pages/anime/anime.specific.vue';
import vpOVA from './modules/pages/ova.page.vue';
import vpMovie from './modules/pages/movie.page.vue';
import vpManga from './modules/pages/manga.page.vue';
import vcAdmin from './modules/admin/Admin.component.vue';

const routes = [
  { path: '/', component: vpHome },
  { path: '/news', component: vpNews },
  { path: '/anime', component: vpAnime,
    children: [
      { path: '', component: vpAnime },
      { path: ':anime', component: vpAnimeSpecific,
        children: [
          { path: 'admin', component: vcAdmin }
        ]
      },
      { path: ':anime/episode/:episode', component: vpAnimeSpecific,
        children: [
          { path: 'admin', component: vcAdmin }
        ]
      }
    ]
  },      
  { path: '/ova', component: vpOVA },
  { path: '/movie', component: vpMovie },
  { path: '/manga', component: vpManga },
  //{ path: '/search', component: vpSearch, props: (route) => ({ query: route.query.q }) },
  { path: '*', component: vp404 }
]

const router = new vueRouter({
  mode: 'history',
  routes: routes
})

module.exports = router;