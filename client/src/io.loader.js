import io from 'socket.io-client/dist/socket.io.slim.js';
io.connection = io.connect('/v2');

io.async = {
  emit(location, data) {
    data = data || { };
    return new Promise((resolve, reject) => {
      io.connection.emit(location, data, (data) => {
        if (data.fail) reject(Error(data.fail));
        else resolve(data);
      })
    })
  }
}

export default io;